import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-dialog-component',
  template: `
    <h1 mat-dialog-title>{{'Attention!'}}</h1>
    <div mat-dialog-content>
      <p>{{ 'Are you sure you want to delete this item?' }}</p>
    </div>
    <div mat-dialog-actions align="end">
      <button mat-button mat-dialog-close cdkFocusInitial>No</button>
      <button mat-button [mat-dialog-close]="data">Yes</button>
    </div>
  `,
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit() {
  }

}
