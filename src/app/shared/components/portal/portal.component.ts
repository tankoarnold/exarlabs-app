import {
  Component,
  OnInit,
  AfterViewInit,
  ComponentFactoryResolver,
  Injector,
  ApplicationRef,
  ViewChild,
  OnDestroy,
  Input
} from '@angular/core';
import {
  CdkPortal,
  PortalOutlet,
  DomPortalOutlet
} from '@angular/cdk/portal';

@Component({
  selector: 'app-portal',
  template: `
  <ng-template cdk-portal>
    <ng-content></ng-content>
  </ng-template>
  `,
  styles: []
})
export class PortalComponent implements OnInit, AfterViewInit, OnDestroy {
  private portalHost: PortalOutlet;
  @ViewChild(CdkPortal, {static: true}) portal;
  @Input()
  targetId: string;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    private appRef: ApplicationRef
  ) {}

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    // Create a portalHost from a DOM element
    this.portalHost = new DomPortalOutlet(
      document.querySelector(this.targetId ? '#' + this.targetId : '#portal-container'),
      this.componentFactoryResolver,
      this.appRef,
      this.injector
    );

    // Attach portal to host
    this.portalHost.attach(this.portal);
  }

  ngOnDestroy(): void {
    this.portalHost.detach();
  }
}
