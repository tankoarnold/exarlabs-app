import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ErrorDialogComponent } from '../components/error-dialog/error-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor(private toast: MatSnackBar, private dialog: MatDialog) { }

  public log(message: string, action?: string): MatSnackBarRef<SimpleSnackBar> {
    return this.toast.open(message, action, {
      duration: 2500,
      verticalPosition: 'bottom',
      horizontalPosition: 'left'
    });
  }

  public error(error: {title: string, message: string}): MatDialogRef<ErrorDialogComponent> {
    return this.dialog.open(ErrorDialogComponent, {
      data: error
    });
  }
}
