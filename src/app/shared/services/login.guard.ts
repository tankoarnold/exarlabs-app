import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { CoreState } from 'src/app/core/state/core.reducer';
import { selectIsLoggedIn } from 'src/app/core/state/core.selectors';
import { tap, map, timeout, debounce, debounceTime } from 'rxjs/operators';
import { AuthService } from 'src/app/core/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.auth.getUser().pipe(
      map((user) => !!user),
      tap((value) => value ? null : this.router.navigateByUrl('/products'))
    );
  }
}
