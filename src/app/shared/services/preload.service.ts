import { Injectable, Compiler, Injector, Type, ViewContainerRef, NgModuleFactory, Inject } from '@angular/core';
import { InjectionToken } from '@angular/core';
import { LoadChildren } from '@angular/router';

export const lazyWidgets: { path: string, loadChildren: LoadChildren }[] = [
  { path: 'cart', loadChildren: () => import('../../modules/cart/cart.module').then(m => m.CartModule) },
];

export function lazyArrayToObj() {
  const result = {};
  for (const w of lazyWidgets) {
    result[w.path] = w.loadChildren;
  }
  return result;
}

export const LAZY_WIDGETS = new InjectionToken<{ [key: string]: string }>('LAZY_WIDGETS');


@Injectable()
export class PreloadService {

  constructor(
    private injector: Injector,
    private compiler: Compiler,
    @Inject(LAZY_WIDGETS) private lazyWidgets: { [key: string]: () => Promise<NgModuleFactory<any> | Type<any>> }) { }


  async load(path: string, container?: ViewContainerRef) {
    const ngModuleOrNgModuleFactory = await this.lazyWidgets[path]();

    let moduleFactory;

    if (ngModuleOrNgModuleFactory instanceof NgModuleFactory) {
      moduleFactory = ngModuleOrNgModuleFactory;
    } else {
      moduleFactory = await this.compiler.compileModuleAsync(ngModuleOrNgModuleFactory);
    }
    const moduleRef = moduleFactory.create(this.injector);
    if (container) {
      const entryComponent = (moduleFactory.moduleType as any).entry;

      const compFactory = moduleRef.componentFactoryResolver.resolveComponentFactory(entryComponent);

      const comp = container.createComponent(compFactory);
    }
  }
}
