import { NgModule } from '@angular/core';
import { PortalComponent } from '../components/portal/portal.component';
import { TestComponent } from '../components/test/test.component';
import { ErrorDialogComponent } from '../components/error-dialog/error-dialog.component';
import { MaterialModule } from './material.module';
import { LAZY_WIDGETS, lazyArrayToObj, PreloadService, lazyWidgets } from '../services/preload.service';
import { provideRoutes } from '@angular/router';
import { ConfirmDialogComponent } from '../components/confirm-dialog-component/confirm-dialog.component';

@NgModule({
  imports: [MaterialModule],
  exports: [PortalComponent, TestComponent, ErrorDialogComponent, MaterialModule, ConfirmDialogComponent],
  declarations: [PortalComponent, TestComponent, ErrorDialogComponent, ConfirmDialogComponent],
  providers: [
    { provide: LAZY_WIDGETS, useFactory: lazyArrayToObj },
    PreloadService,
    provideRoutes(lazyWidgets)
  ],
  entryComponents: [ErrorDialogComponent, ConfirmDialogComponent]
})
export class SharedModule { }
