import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { CoreState, coreReducer, coreFeatureKey } from '../core/state/core.reducer';

// tslint:disable-next-line: no-empty-interface
export interface AppState {
  [coreFeatureKey]: CoreState;
}

export const reducers: ActionReducerMap<AppState> = {
  core: coreReducer,
};


export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
