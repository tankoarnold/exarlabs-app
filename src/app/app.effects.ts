import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { merge, of, fromEvent } from 'rxjs';
import { map, mapTo, switchMap, filter } from 'rxjs/operators';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, ResolveEnd, GuardsCheckEnd } from '@angular/router';
import { CoreActions } from './core/state/core.action-types';
import { BreakpointObserver, MediaMatcher } from '@angular/cdk/layout';

@Injectable()
export class AppEffects {

  online$ = createEffect(() => merge(
    of(navigator.onLine),
    fromEvent(window, 'online').pipe(mapTo(true)),
    fromEvent(window, 'offline').pipe(mapTo(false)),
  ).pipe(map(online => online ? console.log('Online 🟢') : console.log('Offline 🔴'))), { dispatch: false });

  layout$ = createEffect(() => this.bs.observe([
    '(max-width: 600px)',
    '(max-width: 1200px) and (min-width: 601px)',
    '(min-width: 1201px)'
    // '(orientation: portrait)',
    // '(orientation: landscape)',
  ]).pipe(map(() => {
    let layout = '';
    layout = this.media.matchMedia('(max-width: 600px)').matches ? 'mobile' : layout;
    layout = this.media.matchMedia('(max-width: 1200px) and (min-width: 601px)').matches ? 'tablet' : layout;
    layout = this.media.matchMedia('(min-width: 1201px)').matches ? 'desktop' : layout;
    return CoreActions.changeLayout({ layout });
  }))
  );

  navigationStart$ = createEffect(() => this.router.events.pipe(
    filter((value) => value instanceof NavigationStart),
    map(() => CoreActions.navigationStart())
  ));

  navigationEnd$ = createEffect(() => this.router.events.pipe(
    filter((value) => value instanceof NavigationEnd ||
      value instanceof NavigationError ||
      value instanceof NavigationCancel),
    map(() => CoreActions.navigationEnd())
  ));

  constructor(
    private actions$: Actions,
    private router: Router,
    private bs: BreakpointObserver,
    private media: MediaMatcher) {
  }
}
