import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestComponent } from './shared/components/test/test.component';
import { LoginGuard } from './shared/services/login.guard';
import { RoleGuard } from './shared/services/role.guard';

const routes: Routes = [
  { path: '', redirectTo: 'products', pathMatch: 'full' },
  {
    path: 'products',
    loadChildren: () => import('./modules/products/product.module').then((m) => m.ProductModule)
  },
  {
    path: 'cart',
    canActivate: [LoginGuard],
    loadChildren: () => import('./modules/cart/cart.module').then((m) => m.CartModule)
  },
  {
    path: 'admin',
    canActivate: [RoleGuard],
    loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
