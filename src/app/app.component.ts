import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './reducers';
import { Observable } from 'rxjs';
import { selectSidenavState, selectLayoutState, selectNavigationLoading } from './core/state/core.selectors';
import { map } from 'rxjs/operators';
import { CoreActions } from './core/state/core.action-types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'exar-labs-app';
  isOpenSidenav$: Observable<boolean>;
  layout$: Observable<string>;
  navigationLoading$: Observable<boolean>;
  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.isOpenSidenav$ = this.store.select(selectSidenavState);
    this.layout$ = this.store.select(selectLayoutState).pipe(map((layout: string) => {
      switch (layout) {
        case 'mobile':
          return 'over';
        case 'desktop':
          return 'side';
        case 'tablet':
          return 'side';
        default:
          return 'side';
      }
    }));
    this.navigationLoading$ = this.store.select(selectNavigationLoading);
  }

  sidenavStateChange(): void {
    this.store.dispatch(CoreActions.toggleSidenav({ status: false }));
  }
}
