import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AdminState, adminFeatureKey, adminSelectors } from './admin.reducer';

const selectAdminState = createFeatureSelector<AdminState>(adminFeatureKey);

export const selectProducts = createSelector(
  selectAdminState,
  adminSelectors.selectAll
);

export const selectProductsLoading = createSelector(
  selectAdminState,
  state => state.loading
);
