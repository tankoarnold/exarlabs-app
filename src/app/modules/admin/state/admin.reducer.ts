import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { Product } from '../../products/product.models';
import { AdminActions } from './admin.action-types';


export const adminFeatureKey = 'admin';

export interface AdminState extends EntityState<Product> {
  loading: boolean;
}

const adminEntityAdapter = createEntityAdapter<Product>({
  selectId: (product) => product.uid
});

export const initialState: AdminState = adminEntityAdapter.getInitialState({
  loading: true
});

// tslint:disable-next-line: variable-name
const _adminReducer = createReducer(
  initialState,
  on(AdminActions.loadProductsSuccess, (state, { products }) => {
    return adminEntityAdapter.addAll(products, {
      ...state,
      loading: false
    });
  }),
  on(AdminActions.loadProductsFailed, (state) => {
    return {
      ...state,
      loading: false
    };
  }),
  on(AdminActions.addProduct, (state) => {
    return {
      ...state,
      loading: true
    };
  }),
  on(AdminActions.saveProduct, (state) => {
    return {
      ...state,
      loading: true
    };
  }),
  on(AdminActions.addProductSuccess, (state) => {
    return {
      ...state,
      loading: false
    };
  }),
  on(AdminActions.saveProductSuccess, (state) => {
    return {
      ...state,
      loading: false
    };
  }),
  on(AdminActions.addProductFailed, (state) => {
    return {
      ...state,
      loading: false
    };
  }),
  on(AdminActions.saveProductFailed, (state) => {
    return {
      ...state,
      loading: false
    };
  })
);

export const adminSelectors = adminEntityAdapter.getSelectors();

export function adminReducer(state: AdminState | undefined, action: Action) {
  return _adminReducer(state, action);
}
