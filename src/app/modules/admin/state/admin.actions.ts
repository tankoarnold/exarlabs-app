import { createAction, props } from '@ngrx/store';
import { Product } from '../../products/product.models';

export const loadProductsSuccess = createAction(
  '[Admin API] Load products success!',
  props<{ products: Product[] }>()
);

export const loadProductsFailed = createAction(
  '[Admin API] Load products failed!',
  props<{ error?: string }>()
);

export const addProduct = createAction(
  '[Admin page] Add new product!',
  props<{ product: Partial<Product> }>()
);

export const addProductSuccess = createAction(
  '[Admin API] Add new product success!'
);

export const addProductFailed = createAction(
  '[Admin API] Add new product failed!',
  props<{ error?: any }>()
);

export const saveProduct = createAction(
  '[Admin page] Save product!',
  props<{ product: Partial<Product> }>()
);

export const saveProductSuccess = createAction(
  '[Admin API] Save product success!'
);

export const saveProductFailed = createAction(
  '[Admin API] Save product failed!',
  props<{ error?: any }>()
);

export const deleteProduct = createAction(
  '[Admin page] Try delete product!',
  props<{ uid: string }>()
);

export const deleteProductSuccess = createAction(
  '[Admin API] Delete product success!'
);

export const deleteProductCancel = createAction(
  '[Admin page] Delete product cancel!'
);

export const sureDeleteProduct = createAction(
  '[Admin page] Delete product!',
  props<{ uid: string }>()
);

export const deleteProductFailed = createAction(
  '[Admin API] Delete product failed!',
  props<{ error?: any }>()
);
