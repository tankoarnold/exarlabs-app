import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ProductService } from '../../products/product.service';
import { AdminActions } from './admin.action-types';
import { map, catchError, exhaustMap, tap, switchMap, flatMap } from 'rxjs/operators';
import { of, from } from 'rxjs';
import { LoggerService } from 'src/app/shared/services/logger.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog-component/confirm-dialog.component';

@Injectable()
export class AdminEffects {

  loadProducts$ = createEffect(() => this.ps.products().pipe(
    map((products) => AdminActions.loadProductsSuccess({ products })),
    catchError((error) => of(AdminActions.loadProductsFailed({ error })))
  ));

  addProduct$ = createEffect(() => this.actions$.pipe(
    ofType(AdminActions.addProduct),
    exhaustMap(({ product }) => this.ps.addProduct(product).pipe(
      map(() => AdminActions.addProductSuccess()),
      catchError((error) => of(AdminActions.addProductFailed({ error }))
      ))
    )));

  saveProduct$ = createEffect(() => this.actions$.pipe(
    ofType(AdminActions.saveProduct),
    exhaustMap(({ product }) => from(this.ps.saveProduct(product)).pipe(
      map(() => AdminActions.saveProductSuccess()),
      catchError((error) => of(AdminActions.saveProductFailed({ error })))
    ))
  ));

  deleteProduct$ = createEffect(() => this.actions$.pipe(
    ofType(AdminActions.deleteProduct),
    flatMap(({ uid }) => {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: uid,
        disableClose: true
      });
      return dialogRef.afterClosed();
    }),
    map((result: string) => {
      if (result) {
        return AdminActions.sureDeleteProduct({ uid: result });
      }
      return AdminActions.deleteProductCancel();
    })
  ));

  sureDeleteProduct$ = createEffect(() => this.actions$.pipe(
    ofType(AdminActions.sureDeleteProduct),
    exhaustMap(({ uid }) => from(this.ps.deleteProduct(uid)).pipe(
      map(() => AdminActions.deleteProductSuccess()),
      catchError((error) => of(AdminActions.deleteProductFailed({ error })))
    ))
  ));

  deleteProductSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(AdminActions.deleteProductSuccess),
    tap(() => this.logger.log('Product successfully deleted!', 'OK'))
  ), { dispatch: false });

  deleteProductFailed$ = createEffect(() => this.actions$.pipe(
    ofType(AdminActions.deleteProductFailed),
    tap(() => this.logger.error({ title: 'Error!', message: 'An error occurred while deleting the product. Try again!' }))
  ), { dispatch: false });

  addOrSaveProductSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(AdminActions.addProductSuccess, AdminActions.saveProductSuccess),
    tap(() => this.logger.log('Product successfully saved!', 'OK'))
  ), { dispatch: false });

  addOrSaveProductFailed$ = createEffect(() => this.actions$.pipe(
    ofType(AdminActions.addProductFailed, AdminActions.saveProductFailed),
    tap(() => this.logger.error({ title: 'Error!', message: 'An error occurred while saving the product. Try again!' }))
  ), { dispatch: false });

  constructor(
    private actions$: Actions,
    private ps: ProductService,
    private logger: LoggerService,
    private dialog: MatDialog
  ) { }

}
