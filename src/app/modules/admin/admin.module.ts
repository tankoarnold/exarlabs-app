import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { RouterModule } from '@angular/router';
import { AdminContainerComponent } from './components/admin-container/admin-container.component';
import { AdminComponent } from './components/admin/admin.component';
import { EffectsModule } from '@ngrx/effects';
import { AdminEffects } from './state/admin.effects';
import { StoreModule } from '@ngrx/store';
import { adminFeatureKey, adminReducer } from './state/admin.reducer';
import { ProductDialogComponent } from './components/product-dialog/product-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [AdminContainerComponent, AdminComponent, ProductDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    StoreModule.forFeature(adminFeatureKey, adminReducer),
    EffectsModule.forFeature([AdminEffects])
  ],
  entryComponents: [ProductDialogComponent]
})
export class AdminModule { }
