import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Product } from 'src/app/modules/products/product.models';

@Component({
  selector: 'app-admin',
  template: `
  <table mat-table [dataSource]="dataSource">
    <!-- Name Column -->
    <ng-container matColumnDef="name">
      <th mat-header-cell *matHeaderCellDef> Name </th>
      <td mat-cell *matCellDef="let element"> {{element.name}} </td>
    </ng-container>
    <!-- Weight Column -->
    <ng-container matColumnDef="description">
      <th mat-header-cell *matHeaderCellDef> Description </th>
      <td mat-cell *matCellDef="let element"> {{element.description}} </td>
    </ng-container>
    <!-- Symbol Column -->
    <ng-container matColumnDef="price">
      <th mat-header-cell *matHeaderCellDef> Price </th>
      <td mat-cell *matCellDef="let element"> {{element.price + '$'}} </td>
    </ng-container>
    <!-- Update Column -->
    <ng-container matColumnDef="update">
      <th mat-header-cell *matHeaderCellDef></th>
      <td mat-cell *matCellDef="let element">
        <button (click)="updateAction.emit(element)" matTooltip="Update product!" mat-icon-button color="primary">
          <mat-icon>edit</mat-icon>
        </button>
      </td>
    </ng-container>
    <!-- Delete Column -->
    <ng-container matColumnDef="delete">
      <th mat-header-cell *matHeaderCellDef></th>
      <td mat-cell *matCellDef="let element">
        <button (click)="deleteAction.emit(element.uid)" matTooltip="Delete product!" mat-icon-button color="warn">
          <mat-icon>delete</mat-icon>
        </button>
      </td>
    </ng-container>

    <tr mat-header-row *matHeaderRowDef="displayedColumns; sticky: true"></tr>
    <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
  </table>
  `,
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {

  @Input()
  displayedColumns: string[];

  @Input()
  dataSource: Product[];

  @Output()
  updateAction: EventEmitter<Product> = new EventEmitter();

  @Output()
  deleteAction: EventEmitter<string> = new EventEmitter();

}
