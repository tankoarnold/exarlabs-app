import { Component, OnInit, Inject, Optional, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from 'src/app/modules/products/product.models';
import { Observable, Subscription } from 'rxjs';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AdminState } from '../../state/admin.reducer';
import { AdminActions } from '../../state/admin.action-types';
import { Actions, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { selectProductsLoading } from '../../state/admin.selectos';

@Component({
  selector: 'app-product-dialog',
  template: `
    <h1 mat-dialog-title>{{ data ? 'Edit product!' : 'Add new product!' }}</h1>
    <mat-divider class="full-width-divider"></mat-divider>
    <div mat-dialog-content>
      <form [formGroup]="productForm" (ngSubmit)="save()">
        <mat-form-field>
          <mat-label>Name</mat-label>
          <input required
            formControlName="name"
            type="text"
            matInput
            cdkFocusInitial>
          <mat-error>{{ getError(name) }}</mat-error>
        </mat-form-field>
        <mat-form-field>
          <mat-label>Description</mat-label>
          <textarea required
            rows="3"
            formControlName="description"
            matInput>
          </textarea>
          <mat-error>{{ getError(description) }}</mat-error>
        </mat-form-field>
        <mat-form-field>
          <mat-label>Price</mat-label>
          <input required
            formControlName="price"
            type="number"
            min="0"
            matInput>
          <mat-error>{{ getError(price) }}</mat-error>
        </mat-form-field>
      </form>
    </div>
    <mat-divider class="full-width-divider"></mat-divider>
    <div mat-dialog-actions>
      <button mat-button mat-dialog-close>Cancel</button>
      <button (click)="save()" [disabled]="productForm.invalid || productForm.pristine" mat-flat-button color="primary">
        Save
      </button>
    </div>
    <div class="loading-indicator" *ngIf="loading$ | async">
      <mat-progress-bar color="primary" mode="indeterminate"></mat-progress-bar>
    </div>
  `,
  styleUrls: ['./product-dialog.component.scss']
})
export class ProductDialogComponent implements OnInit, OnDestroy {

  name: FormControl = new FormControl(
    this.data ? this.data.name : '',
    Validators.compose([Validators.required])
  );
  description: FormControl = new FormControl(
    this.data ? this.data.description : '',
    Validators.compose([Validators.required])
  );
  price: FormControl = new FormControl(
    this.data ? this.data.price : 0,
    Validators.compose([Validators.required, Validators.min(0)])
  );
  productForm: FormGroup = new FormGroup({
    name: this.name,
    description: this.description,
    price: this.price
  });

  loading$: Observable<boolean>;
  actionSub: Subscription;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Product,
    private dialogRef: MatDialogRef<ProductDialogComponent>,
    private store: Store<AdminState>,
    private actions$: Actions
  ) { }

  ngOnInit() {
    this.actionSub = this.actions$.pipe(
      ofType(AdminActions.addProductSuccess, AdminActions.saveProductSuccess),
      tap(() => this.dialogRef.close())
    ).subscribe();
    this.loading$ = this.store.select(selectProductsLoading);
  }

  ngOnDestroy(): void {
    this.actionSub.unsubscribe();
  }

  save(): void {
    if (this.productForm.valid) {
      if (this.data) {
        const product: Partial<Product> = {
          name: this.name.value,
          description: this.description.value,
          price: this.price.value,
          uid: this.data.uid
        };
        this.store.dispatch(AdminActions.saveProduct({ product }));
      } else {
        const product: Partial<Product> = {
          name: this.name.value,
          description: this.description.value,
          price: this.price.value,
        };
        this.store.dispatch(AdminActions.addProduct({
          product
        }));
      }
    }
  }

  getError(control: FormControl): string {
    return control.hasError('required') ? 'This field is required!' :
      control.hasError('min') ? 'The minimum value is 0!' : '';
  }

}
