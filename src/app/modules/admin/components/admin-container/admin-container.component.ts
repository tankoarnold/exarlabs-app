import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/modules/products/product.models';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { selectProducts, selectProductsLoading } from '../../state/admin.selectos';
import { AdminState } from '../../state/admin.reducer';
import { ProductService } from 'src/app/modules/products/product.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ProductDialogComponent } from '../product-dialog/product-dialog.component';
import { AdminActions } from '../../state/admin.action-types';

@Component({
  selector: 'app-admin-container',
  template: `
    <h1 class="header">Product administration</h1>
    <section>
      <app-admin [dataSource]="dataSource$ | async"
        (deleteAction)="deleteAction($event)"
        (updateAction)="updateAction($event)"
        [displayedColumns]="displayedColumns">
      </app-admin>
    </section>
    <button (click)="addAction()" class="add-fab" mat-fab color="primary">
      <mat-icon>add</mat-icon>
      <span class="add-fab__label">Add new product!</span>
    </button>
  `,
  styleUrls: ['./admin-container.component.scss']
})
export class AdminContainerComponent implements OnInit {

  dataSource$: Observable<Product[]>;
  loading$: Observable<boolean>;

  displayedColumns: string[] = [
    'name',
    'description',
    'price',
    'update',
    'delete'
  ];

  constructor(private store: Store<AdminState>, private dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource$ = this.store.select(selectProducts);
    this.loading$ = this.store.select(selectProductsLoading);
  }

  deleteAction(uid: string): void {
    this.store.dispatch(AdminActions.deleteProduct({ uid }));
  }

  updateAction(product: Product): void {
    this.openProductDialog(product);
  }

  addAction(): void {
    this.openProductDialog();
  }

  openProductDialog(product?: Product): MatDialogRef<ProductDialogComponent, any> {
    return this.dialog.open(ProductDialogComponent, {
      disableClose: true,
      data: product,
      minWidth: '320px',
      panelClass: 'relative-dialog-panel',
    });
  }
}
