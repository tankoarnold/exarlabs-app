import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ProductService } from '../product.service';
import { map, catchError } from 'rxjs/operators';
import { ProductActions } from './product.action-types';
import { of } from 'rxjs';

@Injectable()
export class ProductEffects {

  products$ = createEffect(() => this.ps.products().pipe(
    map((products) => ProductActions.getProductsSuccess({ products })),
    catchError((error) => of(ProductActions.getProductsFailed({ error })))
  ));

  constructor(private actions$: Actions, private ps: ProductService) {}

}
