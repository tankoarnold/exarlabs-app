import { createAction, props } from '@ngrx/store';
import { Product } from '../product.models';

export const getProductsSuccess = createAction(
  '[Product API] Get products success!',
  props<{ products: Product[] }>()
);

export const getProductsFailed = createAction(
  '[Product API] Get products failed!',
  props<{ error?: string }>()
);
