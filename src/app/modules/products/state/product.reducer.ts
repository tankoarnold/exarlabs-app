import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { Product } from '../product.models';
import { ProductActions } from './product.action-types';
import { CartActions } from '../../cart/state/cart.action-types';

export const productFeatureKey = 'products';

export interface ProductsState extends EntityState<Product> {
  loading: boolean;
}

const productEntityAdapter = createEntityAdapter<Product>({
  selectId: (entity) => entity.uid
});

export const initialState: ProductsState = productEntityAdapter.getInitialState({
  loading: true,
});

// tslint:disable-next-line: variable-name
const _productReducer = createReducer(
  initialState,
  on(ProductActions.getProductsSuccess, (state, { products }) => {
    return productEntityAdapter.addAll(products, {
      ...state,
      loading: false
    });
  }),
  on(CartActions.addToCart, (state, { uid }) => {
    return productEntityAdapter.updateOne({
      id: uid,
      changes: {
        loading: true,
      }
    }, state);
  }),
  on(CartActions.addToCartSuccess, (state, { item }) => {
    return productEntityAdapter.updateOne({
      id: item.uid,
      changes: {
        loading: false,
      }
    }, state);
  }),
  on(CartActions.addToCartFailed, (state, { uid }) => {
    return productEntityAdapter.updateOne({
      id: uid,
      changes: {
        loading: false,
      }
    }, state);
  })
);

export const selectAllProduct = productEntityAdapter.getSelectors().selectAll;

export function productReducer(state: ProductsState | undefined, action: Action) {
  return _productReducer(state, action);
}
