import { createFeatureSelector, createSelector } from '@ngrx/store';
import { productFeatureKey, ProductsState, selectAllProduct } from './product.reducer';

const selectProductState = createFeatureSelector<ProductsState>(productFeatureKey);

export const selectProducts = createSelector(
  selectProductState,
  selectAllProduct
);

export const selectProductsLoading = createSelector(
  selectProductState,
  state => state.loading
);
