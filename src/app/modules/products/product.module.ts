import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { ProductContainerComponent } from './components/product-container/product-container.component';
import { StoreModule } from '@ngrx/store';
import { productReducer, productFeatureKey } from './state/product.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ProductEffects } from './state/product.effects';
import { ProductComponent } from './components/product/product.component';
import { MaterialModule } from 'src/app/shared/modules/material.module';

@NgModule({
  declarations: [ProductContainerComponent, ProductComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    MaterialModule,
    StoreModule.forFeature(productFeatureKey, productReducer),
    EffectsModule.forFeature([ProductEffects])
  ]
})
export class ProductModule { }
