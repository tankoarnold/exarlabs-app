export interface Product {
  uid: string;
  name: string;
  description: string;
  price: number;
  loading?: boolean;
}
