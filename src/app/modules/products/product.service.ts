import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Product } from './product.models';
import { Observable, Observer, from } from 'rxjs';
import { map, tap, share } from 'rxjs/operators';
import { LoggerService } from 'src/app/shared/services/logger.service';

const collection = '/products';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private db: AngularFirestore, private logger: LoggerService) { }

  products(): Observable<Product[]> {
    return this.db.collection<Product>(collection).snapshotChanges().pipe(map((actions) => {
      return actions.map((action) => ({ uid: action.payload.doc.id, ...action.payload.doc.data() }));
    }), share());
  }

  addProduct(product: Partial<Product>): Observable<any> {
    return from(this.db.collection(collection).add(product));
  }

  async saveProduct(product: Partial<Product>): Promise<any> {
    // tslint:disable-next-line: prefer-const
    let batch = this.db.firestore.batch();
    const productRef = this.db.firestore.collection(collection).doc(product.uid);
    const carts = await this.db.collection('/carts').get().toPromise();
    for (let i = 0; i < carts.docs.length; i++) {
      const prodRef = this.db.firestore.collection('/carts').doc(carts.docs[i].id).collection('/rows').doc(product.uid);
      const isPresent = await this.db.collection('/carts').doc(carts.docs[i].id).collection('/rows').doc(product.uid).get().toPromise();
      if (isPresent.exists) {
        batch.update(
          prodRef
          ,
          {
            name: product.name,
            description: product.description,
            price: product.price
          }
        );
      }
    }
    batch.update(productRef, {
      name: product.name,
      description: product.description,
      price: product.price
    });
    return batch.commit();
  }

  async deleteProduct(uid: string): Promise<any> {
    let batch = this.db.firestore.batch();
    const productRef = this.db.firestore.collection(collection).doc(uid);
    const carts = await this.db.collection('/carts').get().toPromise();
    for (let i = 0; i < carts.docs.length; i++) {
      const prodRef = this.db.firestore.collection('/carts').doc(carts.docs[i].id).collection('/rows').doc(uid);
      const isPresent = await this.db.collection('/carts').doc(carts.docs[i].id).collection('/rows').doc(uid).get().toPromise();
      if (isPresent.exists) {
        batch.delete(prodRef);
      }
    }
    batch.delete(productRef);
    return batch.commit();
  }
}
