import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../../product.models';

@Component({
  selector: 'app-product',
  template: `
    <mat-card class="mat-elevation-z0 product-card">
      <mat-card-header class="product-card__header">
        <mat-card-title>
          {{ product?.name }}
        </mat-card-title>
        <mat-card-subtitle>
          {{ product?.description }}
        </mat-card-subtitle>
      </mat-card-header>
      <mat-divider [vertical]="true"></mat-divider>
      <mat-card-content class="product-card__content">
        <p>Price:</p>
        <strong> {{ product?.price }} $ </strong>
      </mat-card-content>
      <mat-divider *ngIf="isLoggedIn" [vertical]="true"></mat-divider>
      <mat-card-actions *ngIf="isLoggedIn" class="product-card__action">
        <button *ngIf="!product.loading; else spinner;"
          (click)="addToCartAction()"
          mat-icon-button
          color="primary"
          matTooltip="Add to cart!">
            <mat-icon>shopping_cart</mat-icon>
        </button>
        <ng-template #spinner>
          <mat-progress-spinner diameter="40" mode="indeterminate"></mat-progress-spinner>
        </ng-template>
      </mat-card-actions>
    </mat-card>
  `,
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  @Input()
  product: Product;

  @Input()
  isLoggedIn: boolean;

  @Output()
  addToCart: EventEmitter<Product> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  addToCartAction(): void {
    this.addToCart.emit(this.product);
  }

}
