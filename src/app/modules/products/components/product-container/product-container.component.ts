import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../../product.models';
import { Store } from '@ngrx/store';
import { ProductsState } from '../../state/product.reducer';
import { selectProducts, selectProductsLoading } from '../../state/product.selectors';
import { selectIsLoggedIn } from 'src/app/core/state/core.selectors';
import { CartActions } from 'src/app/modules/cart/state/cart.action-types';

@Component({
  selector: 'app-product-container',
  template: `
    <h1 class="header">Products</h1>
    <section>
      <ng-container *ngIf="(loading$ | async) === false; else loading;">
        <div *ngFor="let product of products$ | async; trackBy: trackByUid">
          <app-product [product]="product"
            (addToCart)="addToCart($event)"
            [isLoggedIn]="isLoggedIn$ | async"></app-product>
          <mat-divider></mat-divider>
        </div>
      </ng-container>
      <ng-template #loading>
        <div class="load-indicator-container">
          <mat-progress-spinner diameter="64" mode="indeterminate"></mat-progress-spinner>
        </div>
      </ng-template>
    </section>
  `,
  styleUrls: ['./product-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductContainerComponent implements OnInit {

  products$: Observable<Product[]>;
  loading$: Observable<boolean>;
  isLoggedIn$: Observable<boolean>;

  constructor(private store: Store<ProductsState>) { }

  ngOnInit() {
    this.products$ = this.store.select(selectProducts);
    this.isLoggedIn$ = this.store.select(selectIsLoggedIn);
    this.loading$ = this.store.select(selectProductsLoading);
  }

  trackByUid(product: Product): string {
    return product.uid;
  }

  addToCart({ uid, name, price }: Product): void {
    this.store.dispatch(CartActions.addToCart({ uid, name, price }));
  }

}
