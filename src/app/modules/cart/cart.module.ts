import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CartRoutingModule } from './cart-routing.module';
import { CartContainerComponent } from './components/cart-container/cart-container.component';
import { StoreModule } from '@ngrx/store';
import { cartFeatureKey, cartReducer } from './state/cart.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CartEffects } from './state/cart.effects';
import { CartComponent } from './components/cart/cart.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/modules/shared.module';

@NgModule({
  declarations: [CartContainerComponent, CartComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    StoreModule.forFeature(cartFeatureKey, cartReducer),
    CartRoutingModule,
    EffectsModule.forFeature([CartEffects])
  ]
})
export class CartModule { }
