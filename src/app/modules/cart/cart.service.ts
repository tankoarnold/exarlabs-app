import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, from, of } from 'rxjs';
import { CartItem } from './cart.models';
import { map, exhaustMap, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { CartState } from './state/cart.reducer';
import { selectIsLoggedIn } from 'src/app/core/state/core.selectors';
import { selectCartItemsCount } from './state/cart.selectors';

const cartCollection = '/carts';
const cartRowsCollection = '/rows';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private db: AngularFirestore, private store: Store<CartState>) { }

  public get getCartCount(): Observable<number> {
    return this.store.select(selectIsLoggedIn).pipe(switchMap((value) => value ? this.store.select(selectCartItemsCount) : of(null)));
  }

  getCartItems(uid: string): Observable<CartItem[]> {
    return this.cartQuerry(uid)
      .snapshotChanges()
      .pipe(map((actions) => {
        return actions.map((action) => ({ uid: action.payload.doc.id, ...action.payload.doc.data() }));
      }));
  }

  removeCartItem(userId: string, uid: string): Observable<any> {
    return from(this.cartQuerry(userId).doc(uid).delete());
  }

  addToCart({ userId, uid, name, price }): Observable<CartItem> {
    return this.cartQuerry(userId).doc(uid).get().pipe(
      exhaustMap((value: firebase.firestore.DocumentSnapshot) => {
        if (value.exists) {
          return this.updateElement(userId, uid);
        } else {
          return this.createElement(userId, { uid, name, price, count: 1 });
        }
      })
    );
  }

  createElement(userId: string, cartItem: CartItem): Observable<CartItem> {
    return from(this.cartQuerry(userId).doc(cartItem.uid).set(cartItem)).pipe(map(() => cartItem));
  }

  updateElement(userId: string, uid: string): Observable<CartItem> {
    return from(this.cartQuerry(userId).doc<CartItem>(uid).get().pipe(
      exhaustMap((action, index) => {
        const cartItem: CartItem  = {
          uid,
          name: (action.data() as CartItem).name,
          count: (action.data() as CartItem).count + 1,
          price: (action.data() as CartItem).price
        };
        return from(this.cartQuerry(userId).doc<CartItem>(uid).update({
          count: cartItem.count
        })).pipe(
          exhaustMap(() => of(cartItem))
        );
      })
    ));
  }

  cartQuerry(uid: string): AngularFirestoreCollection<CartItem> {
    return this.db.collection(cartCollection)
      .doc(uid)
      .collection<CartItem>(cartRowsCollection);
  }
}
