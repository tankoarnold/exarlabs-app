import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { CartItem } from '../cart.models';
import { CartActions } from './cart.action-types';


export const cartFeatureKey = 'cart';

export interface CartState extends EntityState<CartItem> {
  loading: boolean;
  loaded: boolean;
}

const cartEntityAdapter = createEntityAdapter<CartItem>({
  selectId: (item) => item.uid
});

export const initialState: CartState = cartEntityAdapter.getInitialState({
  loading: true,
  loaded: false,
});

// tslint:disable-next-line: variable-name
const _cartReducer = createReducer(
  initialState,
  on(CartActions.getCartItemsSuccess, (state, { cartItems }) => {
    if (cartItems.length < 1) {
      return cartEntityAdapter.removeAll({
        ...state,
        loaded: true,
        loading: false
      });
    }
    return cartEntityAdapter.addAll(cartItems, {
      ...state,
      loaded: true,
      loading: false
    });
  }),
  on(CartActions.getCartItemsFailed, (state) => {
    return {
      ...state,
      loading: false,
      loaded: false
    };
  }),
  on(CartActions.noUser, (state) => {
    return {
      ...state,
      loaded: true,
      loading: false,
    };
  }),
  on(CartActions.removeItem, (state, { uid }) => {
    return cartEntityAdapter.updateOne({
      id: uid,
      changes: {
        loading: true
      }
    }, state);
  }),
  on(CartActions.removeItemSuccess, (state, { uid }) => {
    return cartEntityAdapter.removeOne(uid, state);
  }),
  on(CartActions.removeItemFailed, (state, { uid }) => {
    return cartEntityAdapter.updateOne({
      id: uid,
      changes: {
        loading: false
      }
    }, state);
  }),
);

export const cartSelectors = cartEntityAdapter.getSelectors();

export function cartReducer(state: CartState | undefined, action: Action) {
  return _cartReducer(state, action);
}
