import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { CartService } from '../cart.service';
import { withLatestFrom, map, switchMap, catchError, exhaustMap, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/reducers';
import { selectUser } from 'src/app/core/state/core.selectors';
import { CartItem } from '../cart.models';
import { of } from 'rxjs';
import { CartActions } from './cart.action-types';
import { LoggerService } from 'src/app/shared/services/logger.service';



@Injectable()
export class CartEffects {

  cartInit$ = createEffect(() => this.store$.select(selectUser).pipe(
    switchMap((user) => {
      if (!user) {
        return of(CartActions.noUser({}));
      }
      return this.cs.getCartItems(user.uid).pipe(
        map((cartItems: CartItem[]) => CartActions.getCartItemsSuccess({ cartItems })),
        catchError((error) => of(CartActions.getCartItemsFailed({ error })))
      );
    })
  ));

  removeItemFromCart$ = createEffect(() => this.actions$.pipe(
    ofType(CartActions.removeItem),
    withLatestFrom(this.store$.select(selectUser)),
    exhaustMap(({ [0]: { uid }, [1]: user }) => this.cs.removeCartItem(user.uid, uid).pipe(
      map(() => CartActions.removeItemSuccess({ uid })),
      catchError((error) => of(CartActions.removeItemFailed({ uid, error })))
    ))
  ));

  removeItemFromCartFailed$ = createEffect(() => this.actions$.pipe(
    ofType(CartActions.removeItemFailed),
    tap(() => this.logger.error({ title: 'Error!', message: 'An error occurred while removing the product from cart. Try again!' }))
  ), { dispatch: false });

  addToCart$ = createEffect(() => this.actions$.pipe(
    ofType(CartActions.addToCart),
    withLatestFrom(this.store$.select(selectUser)),
    exhaustMap(({ [0]: { uid, name, price }, [1]: user }) => this.cs.addToCart({ userId: user.uid, uid, name, price }).pipe(
      map((item: CartItem) => CartActions.addToCartSuccess({ item })),
      catchError((error) => of(CartActions.addToCartFailed({ uid, error })))
    ))
  ));

  addToCartSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(CartActions.addToCartSuccess),
    tap(({ item }) => this.logger.log(`${item.name} successfuly added to cart!`))
  ), { dispatch: false });

  addToCartFailed$ = createEffect(() => this.actions$.pipe(
    ofType(CartActions.addToCartFailed),
    tap(() => this.logger.error({ title: 'Error!', message: 'An error occurred while adding the product to cart. Try again!' }))
  ), { dispatch: false });

  constructor(private actions$: Actions, private logger: LoggerService, private cs: CartService, private store$: Store<AppState>) { }

}
