import { createAction, props } from '@ngrx/store';
import { CartItem } from '../cart.models';

export const getCartItemsSuccess = createAction(
  '[Cart API] Get cart items success!',
  props<{ cartItems: CartItem[] }>()
);

export const getCartItemsFailed = createAction(
  '[Cart API] Get cart items failed!',
  props<{ error?: string }>()
);

export const noUser = createAction(
  '[Cart page] No authenticated user found!',
  props<{ message?: string }>()
);

export const removeItem = createAction(
  '[Cart page] Remove item from cart!',
  props<{ uid: string }>()
);

export const removeItemSuccess = createAction(
  '[Cart API] Remove item from cart success!',
  props<{ uid: string }>()
);

export const removeItemFailed = createAction(
  '[Cart API] Remove item from cart failed!',
  props<{ uid: string, error?: any }>()
);

export const addToCart = createAction(
  '[Product page] Add to cart!',
  props<{ uid: string, price: number, name: string }>()
);

export const addToCartSuccess = createAction(
  '[Product API] Add to cart success!',
  props<{ item: CartItem }>()
);

export const addToCartFailed = createAction(
  '[Product API] Add to cart failed!',
  props<{ uid: string, error?: string }>()
);
