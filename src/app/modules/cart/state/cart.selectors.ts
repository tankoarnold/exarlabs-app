import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CartState, cartFeatureKey, cartSelectors, initialState } from './cart.reducer';

const selectCartState = createFeatureSelector<CartState>(cartFeatureKey);

const selectCartStateSafe = createSelector(
  selectCartState,
  state => state ? state : initialState
);

export const selectCartItems = createSelector(
  selectCartStateSafe,
  cartSelectors.selectAll
);

export const selectCartLoading = createSelector(
  selectCartState,
  state => state.loading
);

export const selectCartLoaded = createSelector(
  selectCartState,
  state => state.loaded
);

export const selectCartItemsCount = createSelector(
  selectCartItems,
  (items) => {
    if (!items) {
      return 0;
    }
    return items.reduce((sum, item) => {
      return sum + item.count;
    }, 0);
  }
);

export const selectCartItemsTotal = createSelector(
  selectCartItems,
  (items) => {
    if (!items) {
      return 0;
    }
    return items.reduce((sum, item) => {
      return sum + (item.count * item.price);
    }, 0);
  }
);
