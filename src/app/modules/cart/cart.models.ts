export interface CartItem {
  uid: string;
  count: number;
  price: number;
  name: string;
  loading?: boolean;
}
