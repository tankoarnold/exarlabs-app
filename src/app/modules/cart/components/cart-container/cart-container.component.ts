import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CartItem } from '../../cart.models';
import { Store } from '@ngrx/store';
import { CartState } from '../../state/cart.reducer';
import {
  selectCartItems,
  selectCartLoading,
  selectCartLoaded,
  selectCartItemsCount,
  selectCartItemsTotal
} from '../../state/cart.selectors';
import { CartActions } from '../../state/cart.action-types';

@Component({
  selector: 'app-cart-container',
  template: `
    <h1 class="header">Cart</h1>
    <section>
      <ng-container *ngIf="(loading$ | async) === false; else loading;">
        <ng-container *ngIf="(cartItems$ | async) as cartItems;">
          <div *ngFor="let cartItem of cartItems; trackBy: trackByUid">
            <app-cart (remove)="remove($event)" [cartItem]="cartItem"></app-cart>
            <mat-divider></mat-divider>
          </div>
          <div *ngIf="cartItems?.length < 1" class="no-cart-content">
            <h2>No items...</h2>
            <button routerLink="/products"
              class="call-to-action"
              mat-raised-button
              color="accent">
                Go to products!
              </button>
          </div>
        </ng-container>
      </ng-container>
      <ng-template #loading>
        <div class="load-indicator-container">
          <mat-progress-spinner diameter="64" mode="indeterminate"></mat-progress-spinner>
        </div>
      </ng-template>
    </section>
    <div class="footer">
      <h2>Items count: {{ (count$ | async) || 0 }}</h2>
      <h2>Total: {{ ((total$ | async) || 0 ) + '$' }}</h2>
    </div>
  `,
  styleUrls: ['./cart-container.component.scss']
})
export class CartContainerComponent implements OnInit {

  cartItems$: Observable<CartItem[]>;
  loading$: Observable<boolean>;
  loaded$: Observable<boolean>;
  count$: Observable<number>;
  total$: Observable<number>;

  constructor(private store: Store<CartState>) { }

  ngOnInit() {
    this.cartItems$ = this.store.select(selectCartItems);
    this.loading$ = this.store.select(selectCartLoading);
    this.loaded$ = this.store.select(selectCartLoaded);
    this.count$ = this.store.select(selectCartItemsCount);
    this.total$ = this.store.select(selectCartItemsTotal);
  }

  trackByUid(cartItem: CartItem): string {
    return cartItem.uid;
  }

  remove(uid: string): void {
    this.store.dispatch(CartActions.removeItem({ uid }));
  }

}
