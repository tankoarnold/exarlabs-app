import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CartItem } from '../../cart.models';

@Component({
  selector: 'app-cart',
  template: `
    <mat-card class="mat-elevation-z0 cart-card">
      <div class="cart-card__count">
        <span>
          {{ cartItem?.count ? cartItem.count + ' X' : 'No count!'  }}
        </span>
      </div>
      <div class="cart-card__name">
        <span>
          {{ cartItem?.name || 'No name' }}
        </span>
      </div>
      <div class="cart-card__price">
        <span matTooltip="The price of one product!">
          {{ cartItem?.price ? cartItem.price + '$' : 'No price!' }}
        </span>
      </div>
      <div class="cart-card__action">
        <button *ngIf="!cartItem.loading else spinner" (click)="removeItem()" mat-icon-button color="primary" matTooltip="Remove!">
          <mat-icon>delete</mat-icon>
        </button>
        <ng-template #spinner>
          <mat-progress-spinner diameter="40" mode="indeterminate"></mat-progress-spinner>
        </ng-template>
      </div>
    </mat-card>
  `,
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  @Input()
  cartItem: CartItem;

  @Output()
  remove: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  removeItem(): void {
    this.remove.emit(this.cartItem.uid);
  }

}
