import { Injectable, NgModuleFactory, Compiler, Injector, SystemJsNgModuleLoader, Optional } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { tap, switchMap, map, exhaustMap, catchError } from 'rxjs/operators';
import { CoreActions } from './core.action-types';
import { Action } from '@ngrx/store';
import { IUser } from '../core.models';
import { Router } from '@angular/router';
import { LoggerService } from 'src/app/shared/services/logger.service';
import { PreloadService } from 'src/app/shared/services/preload.service';

@Injectable()
export class CoreEffects {

  userInit$: Observable<Action> = createEffect(() => this.auth.getUser().pipe(
    switchMap((user: IUser) => {
      if (user) {
        return of(CoreActions.loginSuccess({ user }));
      } else {
        return of(CoreActions.userNotLoggedIn({ redirectUrl: '/product' }));
      }
    })
  ));

  login$ = createEffect(() => this.actions$.pipe(
    ofType(CoreActions.login),
    exhaustMap(({ credentials }) => this.auth.login(credentials).pipe(
      map((user: IUser) => CoreActions.loginSuccess({ user })),
      catchError((error) => of(CoreActions.loginFailed({ error })))
    ))
  ));

  loginSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(CoreActions.loginSuccess),
    map(({ user }) => {
      this.logger.log('You`re logged in!');
      this.preSe.load('cart');
      return CoreActions.getUserRoles({ uid: user.uid });
    })
  ));

  getRoles$ = createEffect(() => this.actions$.pipe(
    ofType(CoreActions.getUserRoles),
    exhaustMap(({ uid }) => this.auth.getUserRoles(uid).pipe(
      map((roles: { admin: boolean }) => CoreActions.getUserRolesSuccess({ roles })),
      catchError((error) => of(CoreActions.getUserRolesFailed({ error })))
    ))
  ));

  loginFailed$ = createEffect(() => this.actions$.pipe(
    ofType(CoreActions.loginFailed),
    tap(() => this.logger.error({ title: 'Login failed!', message: 'You`r email address or password was invalid!' }))
  ), { dispatch: false });

  logout$ = createEffect(() => this.actions$.pipe(
    ofType(CoreActions.logout),
    map(({ redirectUrl }) => {
      if (redirectUrl) {
        this.router.navigateByUrl(redirectUrl);
      }
      this.auth.logout().subscribe().unsubscribe();
      this.logger.log('You`re logged out!');
    })
  ), { dispatch: false });

  constructor(
    private actions$: Actions,
    private auth: AuthService,
    private preSe: PreloadService,
    private router: Router,
    private logger: LoggerService) { }
}
