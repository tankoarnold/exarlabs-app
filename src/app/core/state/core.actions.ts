import { createAction, props } from '@ngrx/store';
import { IUser, Credentials } from '../core.models';

export const toggleSidenav = createAction(
  '[Core sidenav] Toggle sidenav!',
  props<{ status?: boolean }>()
);

export const login = createAction(
  '[Auth login] User login!',
  props<{ credentials: Credentials }>()
);

export const loginSuccess = createAction(
  '[Auth login] User login success!',
  props<{ user: IUser }>()
);

export const loginFailed = createAction(
  '[Auth login] User login failed!',
  props<{ error?: string }>()
);

export const changeLayout = createAction(
  '[App layout] Change layout!',
  props<{ layout: string }>()
);

export const logout = createAction(
  '[Auth logout] User logout!',
  props<{ redirectUrl?: string }>()
);

export const userNotLoggedIn = createAction(
  '[Auth logout] User is not logged in!',
  props<{ redirectUrl?: string }>()
);

export const navigationStart = createAction(
  '[App navigation] Navigation start!'
);

export const navigationEnd = createAction(
  '[App navigation] Navigation end!'
);

export const getUserRoles = createAction(
  '[Auth roles] Get user roles!',
  props<{ uid: string }>()
);

export const getUserRolesSuccess = createAction(
  '[Auth API] Get user roles success!',
  props<{ roles: { admin: boolean } }>()
);

export const getUserRolesFailed = createAction(
  '[Auth API] Get user roles failed!',
  props<{ error?: string }>()
);
