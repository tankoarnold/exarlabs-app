import * as coreReducer from './core.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';

const selectCoreState = createFeatureSelector<coreReducer.CoreState>(coreReducer.coreFeatureKey);

export const selectSidenavState = createSelector(
  selectCoreState,
  state => state.isOpenSidenav
);

export const selectIsLoggedIn = createSelector(
  selectCoreState,
  state => state.isLoggedIn
);

export const selectUser = createSelector(
  selectCoreState,
  state => state.user
);

export const selectAuthLoading = createSelector(
  selectCoreState,
  state => state.authLoading
);

export const selectLayoutState = createSelector(
  selectCoreState,
  state => state.layout
);

export const selectNavigationLoading = createSelector(
  selectCoreState,
  state => state.routerLoading
);
