import { Action, createReducer, on } from '@ngrx/store';
import { CoreActions } from './core.action-types';
import { IUser } from '../core.models';


export const coreFeatureKey = 'core';

export interface CoreState {
  isOpenSidenav: boolean;
  isLoggedIn: boolean;
  authLoading: boolean;
  user: IUser;
  routerLoading: boolean;
  layout: string;
}

export const initialState: CoreState = {
  isOpenSidenav: true,
  isLoggedIn: false,
  authLoading: false,
  user: null,
  routerLoading: false,
  layout: 'desktop'
};

// tslint:disable-next-line: variable-name
const _coreReducer = createReducer(
  initialState,
  on(CoreActions.toggleSidenav, (state, { status }) => {
    return {
      ...state,
      isOpenSidenav: status == null ? !state.isOpenSidenav : status
    };
  }),
  on(CoreActions.login, (state) => {
    return {
      ...state,
      authLoading: true
    };
  }),
  on(CoreActions.loginFailed, (state) => {
    return {
      ...state,
      authLoading: false
    };
  }),
  on(CoreActions.loginSuccess, (state, { user }) => {
    return {
      ...state,
      user,
      isLoggedIn: true,
      authLoading: false,
    };
  }),
  on(CoreActions.userNotLoggedIn, (state) => {
    return {
      ...state,
      user: null,
      isLoggedIn: false,
      authLoading: false,
    };
  }),
  on(CoreActions.logout, (state) => {
    return {
      ...state,
      user: null,
      isLoggedIn: false
    };
  }),
  on(CoreActions.changeLayout, (state, { layout }) => {
    return {
      ...state,
      layout
    };
  }),
  on(CoreActions.navigationStart, (state) => {
    return {
      ...state,
      routerLoading: true
    };
  }),
  on(CoreActions.navigationEnd, (state) => {
    return {
      ...state,
      routerLoading: false
    };
  }),
  on(CoreActions.getUserRolesSuccess, (state, { roles }) => {
    return {
      ...state,
      user: {
        ...state.user,
        roles
      }
    };
  })
);

export function coreReducer(state: CoreState | undefined, action: Action) {
  return _coreReducer(state, action);
}
