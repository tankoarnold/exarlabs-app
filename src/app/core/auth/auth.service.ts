import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Credentials, IUser } from '../core.models';
import { Observable, from, of } from 'rxjs';
import { map, switchMap, exhaustMap, catchError } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { Store } from '@ngrx/store';
import { CoreState } from '../state/core.reducer';

const userCollection = 'users';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private store: Store<CoreState>,
    private dialog: MatDialog) {
  }

  login({ email, password }: Credentials): Observable<IUser> {
    return from(this.afAuth.auth.signInWithEmailAndPassword(email, password)).pipe(map((authState) => {
      if (authState && authState.user) {
        return {
          uid: authState.user.uid,
          email: authState.user.email,
          displayName: authState.user.displayName
        };
      } else {
        return null;
      }
    }));
  }

  openLoginDialog(loginMessage?: string): void {
    this.dialog.open(LoginDialogComponent, {
      disableClose: true,
      minWidth: '320px',
      panelClass: 'relative-dialog-panel',
      data: loginMessage
    });
  }

  getUser(): Observable<IUser> {
    return this.afAuth.authState.pipe(map((state) => {
      if (state) {
        return {
          uid: state.uid,
          displayName: state.displayName,
          email: state.email,
        };
      } else {
        return null;
      }
    }));
  }

  getUserRoles(uid: string): Observable<{ admin: boolean }> {
    return from(this.db.collection(userCollection).doc<IUser>(uid).get()).pipe(map((value) => {
      return (value.data().roles as { admin: boolean });
    }));
  }

  logout(): Observable<void> {
    return from(this.afAuth.auth.signOut());
  }
}
