import { Component, OnInit, OnDestroy, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EMAIL_REGEX } from 'src/app/shared/utils';
import { CoreActions } from '../../state/core.action-types';
import { Store, Action } from '@ngrx/store';
import { CoreState } from '../../state/core.reducer';
import { Observable, Subscription } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { selectAuthLoading } from '../../state/core.selectors';

@Component({
  selector: 'app-login-dialog',
  template: `
    <h1 mat-dialog-title>{{ data || 'Login'}}</h1>
    <mat-divider class="full-width-divider"></mat-divider>
    <div mat-dialog-content>
      <form [formGroup]="loginForm" (ngSubmit)="login()">
        <mat-form-field>
          <mat-label>E-mail</mat-label>
          <input required
            formControlName="email"
            type="text"
            matInput
            cdkFocusInitial>
          <mat-error>{{ hasEmailError }}</mat-error>
        </mat-form-field>
        <mat-form-field>
          <mat-label>Password</mat-label>
          <input required
            formControlName="password"
            type="password"
            matInput>
          <mat-error>{{ hasPasswordError }}</mat-error>
        </mat-form-field>
      </form>
    </div>
    <mat-divider class="full-width-divider"></mat-divider>
    <div mat-dialog-actions>
      <button mat-button mat-dialog-close>Cancel</button>
      <button (click)="login()"
      [disabled]="loginForm.invalid"
      mat-flat-button color="primary">
        Login
      </button>
    </div>
    <div class="loading-indicator" *ngIf="loading$ | async">
      <mat-progress-bar color="primary" mode="indeterminate"></mat-progress-bar>
    </div>
  `,
  styleUrls: ['./login-dialog.component.scss']
})
export class LoginDialogComponent implements OnInit, OnDestroy {

  email: FormControl = new FormControl(
    '',
    Validators.compose([Validators.required, Validators.pattern(EMAIL_REGEX)])
  );

  password: FormControl = new FormControl(
    '',
    Validators.compose([Validators.required, Validators.minLength(6)])
  );

  loginForm: FormGroup = new FormGroup({
    email: this.email,
    password: this.password
  });

  loginSuccessSub: Subscription;
  loading$: Observable<boolean>;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: string,
    public dialogRef: MatDialogRef<LoginDialogComponent>,
    private store: Store<CoreState>,
    private actions$: Actions) { }

  ngOnInit() {
    this.loginSuccessSub = this.actions$.pipe(ofType(CoreActions.loginSuccess)).subscribe(() => {
      this.dialogRef.close();
    });
    this.loading$ = this.store.select(selectAuthLoading);
  }

  ngOnDestroy(): void {
    this.loginSuccessSub.unsubscribe();
  }

  get hasEmailError(): string {
    return this.email.hasError('pattern') ? 'Bad e-mail format!' :
      this.email.hasError('required') ? 'Add you`r email address!' : '';
  }

  get hasPasswordError(): string {
    return this.password.hasError('required') ? 'Add you`r password!' :
      this.password.hasError('minlength') ? 'Password must be at least 6 characters long!' : '';
  }

  login(): void {
    if (this.loginForm.valid) {
      this.store.dispatch(CoreActions.login({
        credentials: { email: this.email.value, password: this.password.value
      } }));
    }
  }

}
