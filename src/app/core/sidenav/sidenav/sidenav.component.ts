import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IUser } from '../../core.models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  template: `
  <mat-nav-list class="nav-list">
    <mat-list-item routerLink="products" routerLinkActive="active">
      <mat-icon matListIcon>folder</mat-icon>
      <a matLine >Products</a>
    </mat-list-item>
    <mat-list-item routerLink="cart" (click)="loginAction()" routerLinkActive="active">
      <mat-icon [matBadge]="cartCount" matBadgeColor="accent" matListIcon>shopping_cart</mat-icon>
      <a matLine >Cart</a>
    </mat-list-item>
    <mat-list-item *ngIf="user?.roles?.admin" routerLink="admin"  routerLinkActive="active">
      <mat-icon matListIcon>settings</mat-icon>
      <a matLine >Administration</a>
    </mat-list-item>
  </mat-nav-list>
  `,
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  @Input()
  user: IUser;

  @Input()
  cartCount: number;

  @Output()
  login: EventEmitter<void> = new EventEmitter();

  constructor(private router: Router) { }

  ngOnInit() {
  }

  loginAction(): void {
    if (this.user) {
      // this.router.navigateByUrl('/cart');
    } else {
      this.login.emit();
    }
  }

}
