import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser } from '../../core.models';
import { Store } from '@ngrx/store';
import { CoreState } from '../../state/core.reducer';
import { selectUser } from '../../state/core.selectors';
import { AuthService } from '../../auth/auth.service';
import { CartService } from 'src/app/modules/cart/cart.service';

@Component({
  selector: 'app-sidenav-container',
  template: `
    <app-sidenav [cartCount]="cartCount$ | async" (login)="login()" [user]="user$ | async"></app-sidenav>
  `,
  styleUrls: ['./sidenav-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidenavContainerComponent implements OnInit {

  user$: Observable<IUser>;
  cartCount$: Observable<number>;

  constructor(private store: Store<CoreState>, private auth: AuthService, private cs: CartService) { }

  ngOnInit() {
    this.user$ = this.store.select(selectUser);
    this.cartCount$ = this.cs.getCartCount;
  }

  login(): void {
    this.auth.openLoginDialog('Please login for use this feature!');
  }

}
