import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { CoreState } from '../../state/core.reducer';
import { CoreActions } from '../../state/core.action-types';
import { Observable } from 'rxjs';
import { selectIsLoggedIn } from '../../state/core.selectors';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-header-container',
  template: `
    <app-header (toggle)="toggleSidenav()"
    (login)="login()"
    (logout)="logout()"
    [isLoggedIn]="isLoggedIn$ | async"></app-header>
  `,
  styleUrls: ['./header-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderContainerComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;

  constructor(private store: Store<CoreState>, private auth: AuthService) { }

  ngOnInit() {
    this.isLoggedIn$ = this.store.select(selectIsLoggedIn);
  }

  toggleSidenav(): void {
    this.store.dispatch(CoreActions.toggleSidenav({}));
  }

  login(): void {
    // this.store.dispatch(CoreActions.login({}));
    this.auth.openLoginDialog();
  }

  logout(): void {
    this.store.dispatch(CoreActions.logout({ redirectUrl: '/products' }));
  }
}
