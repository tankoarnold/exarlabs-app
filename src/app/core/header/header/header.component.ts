import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
  <mat-toolbar color="primary">
    <mat-toolbar-row>
      <div class="wrapper">
        <button (click)="toggleSidenav()" class="wrapper__button" mat-icon-button>
          <mat-icon>menu</mat-icon>
        </button>
        <span>Menu</span>
      </div>
      <div class="spacer"></div>
      <button *ngIf="!isLoggedIn" (click)="loginAction()" mat-button class="toolbar-button">
        <mat-icon>person</mat-icon>
        <span>Login</span>
      </button>
      <button *ngIf="isLoggedIn" (click)="logoutAction()" mat-button class="toolbar-button">
        <mat-icon>close</mat-icon>
        <span>Logout</span>
      </button>
    </mat-toolbar-row>
  </mat-toolbar>
  `,
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output()
  toggle: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  login: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  logout: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  isLoggedIn: boolean;

  constructor() { }

  ngOnInit() {
  }

  toggleSidenav(): void {
    this.toggle.emit();
  }

  loginAction(): void {
    this.login.emit();
  }

  logoutAction(): void {
    this.logout.emit();
  }

}
