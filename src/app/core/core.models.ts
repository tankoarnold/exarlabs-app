export interface Credentials {
  email: string;
  password: string;
}

export interface IUser {
  uid: string;
  displayName: string;
  email: string;
  roles?: { admin: boolean };
}
