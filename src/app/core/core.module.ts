import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderContainerComponent } from './header/header-container/header-container.component';
import { HeaderComponent } from './header/header/header.component';
import { MaterialModule } from '../shared/modules/material.module';
import { SidenavContainerComponent } from './sidenav/sidenav-container/sidenav-container.component';
import { EffectsModule } from '@ngrx/effects';
import { CoreEffects } from './state/core.effects';
import { LoginDialogComponent } from './auth/login-dialog/login-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SidenavComponent } from './sidenav/sidenav/sidenav.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    HeaderContainerComponent,
    HeaderComponent,
    SidenavContainerComponent,
    LoginDialogComponent,
    SidenavComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    EffectsModule.forFeature([CoreEffects]),
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    HeaderContainerComponent,
    SidenavContainerComponent,
    LoginDialogComponent
  ],
  entryComponents: [
    LoginDialogComponent
  ]
})
export class CoreModule { }
